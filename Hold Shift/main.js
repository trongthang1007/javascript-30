const checkboxes = document.querySelectorAll('.inbox input[type="checkbox"]');

let lastChecked;

function handleCheck(e) {
    if (this.nextElementSibling.classList.contains('active')) this.nextElementSibling.classList.remove('active');
    else this.nextElementSibling.classList.add('active');
    let inBetween = false;
    if (e.shiftKey && this.checked) {
        checkboxes.forEach(checkbox => {
            if(checkbox === this || checkbox === lastChecked) {
                inBetween = !inBetween;
            }

            if(inBetween) {
                checkbox.checked = true;
                checkbox.nextElementSibling.classList.add('active');
            }        
        });
    }  
    lastChecked = this;
}

checkboxes.forEach(checkbox => checkbox.addEventListener('click', handleCheck));