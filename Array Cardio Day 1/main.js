const inventors = [
    {first: 'Nguyen', last: 'Van A', year: 1879, passed: 1955},
    {first: 'Tran', last: 'Van B', year: 1643, passed: 1727},
    {first: 'Duong', last: 'Van C', year: 1564, passed: 1642},
    {first: 'Mai', last: 'Van D', year: 1867, passed: 1934},
    {first: 'Vo', last: 'Van E', year: 1571, passed: 1630},
    {first: 'Hao', last: 'Van F', year: 1473, passed: 1543},
    {first: 'Dang', last: 'Van G', year: 1858, passed: 1947},
    
];

const people = ['Beck, Glenn', 'Tran, Van A', 'Nguyen, Van C', 'Dang, Van D',
'Mai, van C', 'Vo, Van D', 'Do, Van E', 'Tran, Van F', 
'Mai, van Q', 'Vo, Van W', 'Do, Van E', 'Tran, Van K', 
'Mai, van O', 'Vo, Van O', 'Do, Van R', 'Tran, Van L', 
'Mai, van P', 'Vo, Van I', 'Do, Van T', 'Tran, Van M', 
'Mai, van Z', 'Vo, Van U', 'Do, Van Y', 'Tran, Van N', ]

// #############
const fifteen = inventors.filter(inventor => inventor.year >= 1500 && inventor.year < 1600);

// #############
const fullNames = inventors.map(inventor => `${inventor.first} ${inventor.last}`);

// #############
const ordered = inventors.sort((a,b) => (a.year > b.year) ? 1 : -1 );

// #############
const totalYear = inventors.reduce((total, inventor) => {
    return total + (inventor.passed - inventor.year);
}, 0);

// #############
const oldest = inventors.sort((a, b) => {
    const lastGuy = a.passed - a.year;
    const nextGuy = b.passed - b.year;
    return lastGuy > nextGuy ? 1 : -1;
});

// #############
// const category = document.querySelector('.mw-category');
// const links = Array.from(category.querySelectorAll('a'));
// const de = links.map(link => link.textContent).filter(streetName => streetName.includes("de"));

// #############
const alpha = people.sort((lastOne, nextOne) => {
    const [first1, last1] = lastOne.split(', ');  
    const [first2, last2] = nextOne.split(', ');  

    return last1 > last2 ? 1 : -1;
});

// #############
const data = ['car', 'car', 'truck', 'truck', 'bike', 'walk', 'car', 'van',
'bike', 'walk', 'car', 'van', 'car', 'truck'];

const transportation = data.reduce((obj, item) => {
    if(!obj[item]) {
        obj[item] = 0;
    }
    obj[item]++;
    return obj;
}, {});

